DROP TABLE IF EXISTS cardcoursedto;
CREATE TABLE IF NOT EXISTS cardcoursedto (
                                             id INT PRIMARY KEY AUTO_INCREMENT,
                                             course_Name VARCHAR(255),
                                             description TEXT,
                                             status VARCHAR(50),
                                             image VARCHAR(255)
);

INSERT INTO cardcoursedto (course_Name, description, status, image)
VALUES ('JAVA', 'JAVA22', '진행 중', 'java.jpg');

INSERT INTO cardcoursedto (course_Name, description, status, image)
VALUES ('SPRING', 'SPRING22', '대기 중', 'SPRING.jpg');

INSERT INTO cardcoursedto (course_Name, description, status, image)
VALUES ('REACT', 'REACT22', '종료', 'REACT.jpg');
