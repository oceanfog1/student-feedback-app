package com.project.feedback.domain;

import com.project.feedback.domain.dto.mainInfo.StatusInfo;
import com.project.feedback.domain.dto.mainInfo.StudentInfo;
import com.project.feedback.fixture.CourseFixture;
import com.project.feedback.fixture.RepositoryFixture;
import com.project.feedback.fixture.task.TaskEntityFixture;
import com.project.feedback.fixture.user.UserFixture;
import com.project.feedback.fixture.user.UserTaskFixture;
import com.project.feedback.infra.outgoing.jpa.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

class CourseStudentTest {

    // repository는 di해서 바로 테스트 가능한데 여기는 만들어지지가 않음

    @Test
    @DisplayName("유저 2명 Task 2개로 로직이 잘 작동 하는지 테스트")
    void courseStudent() {

//        List<UserTaskEntity> userTaskEntities = userTaskPort.findByUserEntityIn(userEntities);
//        Map<Long, GitRepo> repoMap = repositoryPort.findRepositoryMapByName(repoName);

        UserEntity user = UserFixture.dummyUser1(); //findUserByUserName
        CourseEntity course = CourseFixture.courseEntity("BP 백엔드 1기"); // findCourseByUserId
        List<TaskEntity> todayTasks = TaskEntityFixture.taskEntities(); // findByTodayCourseInfo
        List<UserEntity> users = UserFixture.users();
        List<UserTaskEntity> userTaskEntities = UserTaskFixture.userTaskEntities();  //findByUserEntityIn
        Map<Long, GitRepo> repoMap = RepositoryFixture.repositories();

        CourseStudent courseStudent = new CourseStudent(User.fromEntity(user), Course.fromEntity(course), todayTasks, users, userTaskEntities, repoMap);

        List<StudentInfo> studentInfos = courseStudent.getStudentInfo();

        for (StudentInfo studentInfo : studentInfos) {
            for (StatusInfo statusInfo : studentInfo.getStatus()) {
                System.out.printf("%s %s %s\n", studentInfo.getRealName(), statusInfo.getTaskName(), statusInfo.getTaskStatus());
            }
        }

    }
}