package com.project.feedback.fixture;

import com.project.feedback.domain.GitRepo;
import com.project.feedback.domain.dto.repository.RepositoryRequest;
import com.project.feedback.infra.outgoing.jpa.RepositoryEntity;
import com.project.feedback.infra.outgoing.jpa.UserEntity;

import java.util.HashMap;
import java.util.Map;

public class RepositoryFixture {

    public static Map<Long, GitRepo> repositories() {
        Map<Long, GitRepo> repositories = new HashMap<>();



        return repositories;
    }

    public static RepositoryEntity repository(String address, UserEntity user) {
        return RepositoryEntity.builder()
                .id(0L)
                .address(address)
                .user(user)
                .build();
    }
    public static RepositoryEntity securityRepo(UserEntity user) {
        return RepositoryEntity.builder()
                .id(1L)
                .name("security")
                .address("https://www.github.com/username/security")
                .user(user)
                .build();
    }

    public static RepositoryEntity snsRepo(UserEntity user) {
        return RepositoryEntity.builder()
                .id(2L)
                .name("sns")
                .address("https://www.github.com/username/sns")
                .user(user)
                .build();
    }

    public static RepositoryRequest securityRepoRequest() {
        return RepositoryRequest.builder()
                .name("security")
                .address("https://www.github.com/username/security")
                .build();
    }

    public static RepositoryRequest snsRepoRequest() {
        return RepositoryRequest.builder()
                .name("sns")
                .address("https://www.github.com/username/sns")
                .build();
    }
}
