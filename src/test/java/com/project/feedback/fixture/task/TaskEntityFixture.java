package com.project.feedback.fixture.task;

import com.project.feedback.domain.TaskStatus;
import com.project.feedback.infra.outgoing.jpa.TaskEntity;

import java.util.ArrayList;
import java.util.List;

public class TaskEntityFixture {

    public static TaskEntity dummyTaskEntity1() {
        return TaskEntityFixture.taskEntity(0l, "DTO만들기", TaskStatus.CREATED);
    }

    public static TaskEntity dummyTaskEntity2() {
        return TaskEntityFixture.taskEntity(1l, "Entity만들기", TaskStatus.CREATED);
    }

    public static TaskEntity taskEntity(Long id, String title, TaskStatus taskStatus) {
        return TaskEntity.builder()
                .id(id)
                .taskStatus(taskStatus)
                .title(title)
                .build();
    }

    public static List<TaskEntity> taskEntities() {
        List<TaskEntity> tasks = new ArrayList<>();
        tasks.add(dummyTaskEntity1());
        tasks.add(dummyTaskEntity2());
        return tasks;
    }

}
