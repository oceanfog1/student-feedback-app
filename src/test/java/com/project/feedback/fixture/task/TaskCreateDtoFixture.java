package com.project.feedback.fixture.task;

import com.project.feedback.domain.dto.task.TaskCreateRequest;
import com.project.feedback.domain.dto.task.TaskCreateResponse;

public class TaskCreateDtoFixture {

    public static TaskCreateRequest taskCreateRequest(String status) {
        return TaskCreateRequest.builder()
                .title("Task 제목")
                .status(status)
                .build();
    }

    public static TaskCreateResponse taskCreateResponse(Long id, String title) {
        return TaskCreateResponse.builder()
                .taskId(id)
                .title(title)
                .build();
    }
}
