package com.project.feedback.fixture;

import com.project.feedback.infra.outgoing.jpa.CourseEntity;

public class CourseFixture {
    public static CourseEntity courseEntity(String name) {
        return CourseEntity.builder()
                .name(name)
                .build();
    }
}
