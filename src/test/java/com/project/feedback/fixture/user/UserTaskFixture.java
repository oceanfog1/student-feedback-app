package com.project.feedback.fixture.user;

import com.project.feedback.fixture.task.TaskEntityFixture;
import com.project.feedback.infra.outgoing.jpa.TaskEntity;
import com.project.feedback.infra.outgoing.jpa.UserEntity;
import com.project.feedback.infra.outgoing.jpa.UserTaskEntity;

import java.util.ArrayList;
import java.util.List;

public class UserTaskFixture {

    public static UserTaskEntity userTaskEntity(Long id, UserEntity userEntity, TaskEntity taskEntity) {
        return UserTaskEntity.builder()
                .id(id)
                .user(userEntity)
                .taskEntity(taskEntity)
                .status(taskEntity.getTaskStatus())
                .build();
    }

    public static List<UserTaskEntity> userTaskEntities() {
        List<UserTaskEntity> li = new ArrayList<>();
        TaskEntity task1 = TaskEntityFixture.dummyTaskEntity1();
        TaskEntity task2 = TaskEntityFixture.dummyTaskEntity2();
        UserEntity user1 = UserFixture.dummyUser1();
        UserEntity user2 = UserFixture.dummyUser2();

        li.add(userTaskEntity(0l, user1, task1));
        li.add(userTaskEntity(1l, user2, task1));
        li.add(userTaskEntity(2l, user1, task2));
        li.add(userTaskEntity(3l, user2, task2));
        return li;
    }

}
