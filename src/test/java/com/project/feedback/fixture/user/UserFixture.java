package com.project.feedback.fixture.user;

import com.project.feedback.domain.Role;
import com.project.feedback.infra.outgoing.jpa.UserEntity;

import java.util.ArrayList;
import java.util.List;

public class UserFixture {

    public static UserEntity dummyUser1() {
        return UserFixture.userEntity(0l, "dummyuser1", "김미미");
    }
    public static UserEntity dummyUser2() {
        return UserFixture.userEntity(1l, "dummyuser2", "김나나");
    }

    public static UserEntity userEntity(Long id, String name, String realName) {
        return UserEntity.builder()
                .id(id)
                .userName(name)
                .realName(realName)
                .role(Role.ROLE_STUDENT)
                .build();
    }

    public static List<UserEntity> users() {
        List<UserEntity> users = new ArrayList<>();
        users.add(dummyUser1());
        users.add(dummyUser2());
        return users;

    }
}
