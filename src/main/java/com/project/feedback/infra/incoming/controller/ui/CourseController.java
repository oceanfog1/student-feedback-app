package com.project.feedback.infra.incoming.controller.ui;

import com.project.feedback.application.CourseStudentService;
import com.project.feedback.domain.CourseStudent;
import com.project.feedback.domain.dto.CourseStudentRequest;
import com.project.feedback.domain.dto.course.AddStudentRequest;
import com.project.feedback.domain.dto.course.CardCourseDto;
import com.project.feedback.domain.dto.course.CourseCreateRequest;
import com.project.feedback.domain.dto.course.TodayCourseInfo;
import com.project.feedback.domain.dto.mainInfo.FilterInfo;
import com.project.feedback.domain.dto.mainInfo.StudentInfo;
import com.project.feedback.infra.outgoing.jpa.CourseEntity;
import com.project.feedback.infra.outgoing.jpa.UserEntity;
import com.project.feedback.application.CourseServiceImpl;
import com.project.feedback.application.FindService;
import com.project.feedback.repository.CardCourseRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Controller
@RequestMapping("/courses")
@RequiredArgsConstructor
@Slf4j
public class CourseController {
    private final CourseServiceImpl courseService;
    private final FindService findService;
    private final CourseStudentService courseStudentService;

    @GetMapping("/write")
    public String writePage(Model model) {
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        String dateNow = now.format(formatter);
        int cnt = courseService.getCourseLength() + 1;
//        String cnt = String.valueOf(courseService.getCourseLength()+1);
        model.addAttribute("currentPage", "create_course");
        model.addAttribute("cnt", cnt);
        model.addAttribute("date", dateNow);
        model.addAttribute("courseCreateRequest", new CourseCreateRequest());
        return "courses/write";
    }

    @PostMapping("/write")
    public String write(@ModelAttribute CourseCreateRequest req, Authentication auth) {
        courseService.createCourse(req, auth.getName());
        return "redirect:/";
    }


    // course 수정 화면
    @GetMapping("/{courseId}")
    public String show(@PathVariable Long courseId, Model model, Authentication auth,
                       @PageableDefault(size = 20)
                       @SortDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {

        model.addAttribute("course", courseService.findByCourseId2(courseId));

        return "courses/edit";
    }


    @PostMapping("/register")
    public String register(@ModelAttribute("addStudentRequest") AddStudentRequest req, Authentication auth) {
        courseService.registerStudent(req);
        return "redirect:/users";
    }



    /*
    /course/student로 CourseStudent호출 했을 때 가장 먼저 호출 되는 Controller
     */
    @GetMapping("/students")
    public String addCourseId(Authentication auth, RedirectAttributes redirectAttributes, Model model) {
        log.info(SecurityContextHolder.getContext().getAuthentication().getName());
        TodayCourseInfo todayCourseInfo = courseService.getTodayCourseInfo(auth.getName());

        redirectAttributes.addAttribute("courseId", todayCourseInfo.getId());
        redirectAttributes.addAttribute("week", todayCourseInfo.getWeek());
        redirectAttributes.addAttribute("day", todayCourseInfo.getDay());

        return "redirect:{courseId}/students/weeks/{week}/days/{day}";
    }

    @PostMapping("/students")
    public String weekAndDaySubmit(@ModelAttribute FilterInfo filterInfo, Model model, RedirectAttributes redirectAttribute, Authentication auth) {
        log.info("filterInfo:{} {}", filterInfo.getWeek(), filterInfo.getGitRepoName());
        TodayCourseInfo todayCourseInfo = courseService.getTodayCourseInfo(auth.getName());
        redirectAttribute.addAttribute("courseId", todayCourseInfo.getId());
        redirectAttribute.addAttribute("week", filterInfo.getWeek());
        redirectAttribute.addAttribute("day", filterInfo.getDay());
        return "redirect:{courseId}/students/weeks/{week}/days/{day}?repo=" + filterInfo.getGitRepoName();
    }

    @GetMapping("/{courseId}/students/weeks/{week}/days/{day}")
    public String listWeekAndDay(@PathVariable long courseId, @PathVariable long week, @PathVariable long day,
                                 @RequestParam(value = "repo", defaultValue = "") String repo, Authentication auth, Model model) {

        log.info("repo:{}", repo);
        TodayCourseInfo todayCourseInfo = new TodayCourseInfo(courseId, week, (int) day);
        CourseStudent courseStudent = courseStudentService.getCourseStudent(new CourseStudentRequest(todayCourseInfo, repo, auth.getName()));

        List<StudentInfo> studentInfos = courseStudent.getStudentInfo();
        log.info("courseStudent isEmpty:{}", courseStudent.isTaskEmpty());

        model.addAttribute("filterInfo", new FilterInfo(repo));
        model.addAttribute("authName", auth.getName());
        model.addAttribute("week", week);
        model.addAttribute("day", day);
        model.addAttribute("repositoryNames", findService.findAllRepositoryNames());
        model.addAttribute("courseName", courseStudent.getCourse().getName());
        model.addAttribute("taskList", courseStudent.getTasks());
        model.addAttribute("studentTaskList", studentInfos);

        return "courses/students/show";
    }

    @ResponseBody
    @PostMapping("/setWeek")
    public Long calculateWeek(@RequestParam String courseName) {
        CourseEntity courseEntity = findService.findCourseByName(courseName);
        return TodayCourseInfo.fromEntity(courseEntity).getWeek();
    }



    @GetMapping("/temp")
    public String courseList(Model model, @PageableDefault(size = 20, sort = "id", direction = Sort.Direction.DESC) Pageable pageable,
                             Authentication auth) {

        List<CardCourseDto> cardCourses = courseService.listCardCourses();
        log.info("로직 작동");
        model.addAttribute("cardCourses", cardCourses);
        model.addAttribute("coursePage", courseService.getCourses(auth.getName(), pageable));

        // 다른 필요한 데이터도 모델에 추가 가능
        model.addAttribute("currentPage", "temp");

        return "courses/list";
    }

}
