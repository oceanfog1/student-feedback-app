package com.project.feedback.infra.incoming.controller.ui;

import com.project.feedback.application.CourseServiceImpl;
import com.project.feedback.application.FindService;
import com.project.feedback.application.UserService;
import com.project.feedback.domain.Course;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/admin")
@RequiredArgsConstructor
@Slf4j
public class AdminUiController {

    private final FindService findService;
    private final UserService userService;
    private final CourseServiceImpl courseService;

    @GetMapping("/courses")
    public String list(Model model, @PageableDefault(size = 20, sort = "id", direction = Sort.Direction.DESC)
            Pageable pageable) {
        List<Course> courses = courseService.getCourses(pageable);

        model.addAttribute("currentPage", "courses");
        model.addAttribute("courses", courses);
        model.addAttribute("nowPage", pageable.getPageNumber() + 1);
        model.addAttribute("lastPage", pageable.getPageSize());

        return "courses/show";
    }


    @GetMapping(value = {"", "/"})
    public String root(Authentication auth, Model model) {
        return "redirect:admin/dummy";
    }

    @GetMapping(value = {"/dummy"})
    public String dummy(Authentication auth, Model model){
        model.addAttribute("currentPage", "dummy");
        model.addAttribute("courses", findService.findAllCourse());
        return "admin/dummy_objects";
    }


    //Dummy User 등록
    @GetMapping("/users")
    public String addDummyUsers(@RequestParam(name = "userCnt", defaultValue = "0") int userCnt){
        log.info("userCnt:{}", userCnt);
        userService.addDummyUsers(userCnt);
        return "redirect:/users";
    }

    //Dummy Task 등록
    @GetMapping("/tasks")
    public String addDummyTasks(@RequestParam(name = "taskCnt", defaultValue = "0") int taskCnt,
                                @RequestParam(name = "questionCnt", defaultValue = "0") int questionCnt,
                                @RequestParam(name = "courseId") long courseId){
        log.info("courseId:{}", courseId);
        log.info("taskCnt:{}", taskCnt);
        log.info("questionCnt:{}", questionCnt);
        userService.addDummyTasks(courseId, taskCnt, questionCnt);
        return "redirect:/tasks";
    }

}
