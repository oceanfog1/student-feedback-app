package com.project.feedback.infra.outgoing.adapter;

import com.project.feedback.exception.CustomException;
import com.project.feedback.exception.ErrorCode;
import com.project.feedback.infra.outgoing.jpa.CourseUserEntity;
import com.project.feedback.infra.outgoing.jpa.UserEntity;
import com.project.feedback.infra.outgoing.port.UserPort;
import com.project.feedback.infra.outgoing.repository.CourseUserRepository;
import com.project.feedback.infra.outgoing.repository.UserRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserAdapter implements UserPort {
    private final UserRepository userRepository;
    private final CourseUserRepository courseUserRepository;

    public UserAdapter(UserRepository userRepository, CourseUserRepository courseUserRepository) {
        this.userRepository = userRepository;
        this.courseUserRepository = courseUserRepository;
    }


    @Override
    public UserEntity findUserByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new CustomException(ErrorCode.EMAIL_NOT_FOUND));
    }

    @Override
    public UserEntity findUserByUserName(String userName) {
        return userRepository.findByUserName(userName)
                .orElseThrow(() -> new CustomException(ErrorCode.USERNAME_NOT_FOUND));
    }

    /**
     * 로그인한 User(student)가 속한 Course의 학생들 목록을 가져오는 api
     */
    @Override
    public List<UserEntity> findUsersByCourseId(Long courseId) {
        List<CourseUserEntity> courseUserEntities = courseUserRepository.findCourseEntityUserByCourseEntityId(courseId);

        List<UserEntity> users = new ArrayList<>();
        for(CourseUserEntity c : courseUserEntities){
            UserEntity userEntity = c.getUser();
            users.add(userEntity);
        }

        return users;
    }


}
