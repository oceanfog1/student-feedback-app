package com.project.feedback.infra.outgoing.port;

import com.project.feedback.infra.outgoing.jpa.UserEntity;
import com.project.feedback.infra.outgoing.jpa.UserTaskEntity;

import java.util.List;

public interface UserTaskPort {
    List<UserTaskEntity> findByUserEntitiesIn(List<UserEntity> userEntities);
}
