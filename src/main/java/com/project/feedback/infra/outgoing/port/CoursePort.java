package com.project.feedback.infra.outgoing.port;

import com.project.feedback.infra.outgoing.jpa.CourseEntity;
import com.project.feedback.infra.outgoing.jpa.UserEntity;

public interface CoursePort {

    CourseEntity findCourseByName(String courseName);

    CourseEntity findCourseByUserId(UserEntity userEntity);

}
