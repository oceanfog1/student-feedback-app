package com.project.feedback.infra.outgoing.adapter;

import com.project.feedback.exception.CustomException;
import com.project.feedback.exception.ErrorCode;
import com.project.feedback.infra.outgoing.jpa.CourseEntity;
import com.project.feedback.infra.outgoing.jpa.CourseUserEntity;
import com.project.feedback.infra.outgoing.jpa.UserEntity;
import com.project.feedback.infra.outgoing.port.CoursePort;
import com.project.feedback.infra.outgoing.repository.CourseRepository;
import com.project.feedback.infra.outgoing.repository.CourseUserRepository;
import org.springframework.stereotype.Component;

@Component
public class CourseAdapter implements CoursePort {

    private final CourseRepository courseRepository;
    private final CourseUserRepository courseUserRepository;

    public CourseAdapter(CourseRepository courseRepository, CourseUserRepository courseUserRepository) {
        this.courseRepository = courseRepository;
        this.courseUserRepository = courseUserRepository;
    }

    public CourseEntity findCourseByName(String courseName) {
        return courseRepository.findByName(courseName)
                .orElseThrow(() -> new CustomException(ErrorCode.COURSE_NOT_FOUND, String.format("코스명: %s(이)가 없습니다.", courseName)));
    }

    @Override
    public CourseEntity findCourseByUserId(UserEntity userEntity) {
        CourseUserEntity courseUserEntity = courseUserRepository.findCourseEntityUserByUserId(userEntity.getId())
                .orElseThrow(() -> new CustomException(ErrorCode.USER_COURSE_NOT_FOUND, String.format("%s가 속한 코스가 없습니다.", userEntity.getUserName())));
        return courseUserEntity.getCourseEntity();
    }

}
