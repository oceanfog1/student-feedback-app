package com.project.feedback.infra.outgoing.adapter;

import com.project.feedback.domain.GitRepo;
import com.project.feedback.infra.outgoing.jpa.RepositoryEntity;
import com.project.feedback.infra.outgoing.jpa.UserEntity;
import com.project.feedback.infra.outgoing.port.RepositoryPort;
import com.project.feedback.repository.RepositoryRepository;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class RepositoryAdapter implements RepositoryPort {

    private final RepositoryRepository repositoryRepository;

    public RepositoryAdapter(RepositoryRepository repositoryRepository) {
        this.repositoryRepository = repositoryRepository;
    }

    @Override
    public Map<Long, GitRepo> findRepositoryMapByName(String repoName) {
        HashMap<Long, GitRepo> hm = new HashMap<>();
        repositoryRepository.findByName(repoName).forEach(item-> hm.putIfAbsent(item.getUser().getId(), GitRepo.fromEntity(item)));
        return hm;
    }

    @Override
    public List<RepositoryEntity> findRepositoriesByUser(UserEntity user) {
        return repositoryRepository.findByUserOrderByModifiedDateDesc(user);
    }

}
