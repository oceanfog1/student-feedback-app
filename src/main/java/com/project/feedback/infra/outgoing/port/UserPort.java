package com.project.feedback.infra.outgoing.port;

import com.project.feedback.infra.outgoing.jpa.UserEntity;

import java.util.List;


public interface UserPort {
    UserEntity findUserByEmail(String email);

    UserEntity findUserByUserName(String userName);

    List<UserEntity> findUsersByCourseId(Long courseId);
}
