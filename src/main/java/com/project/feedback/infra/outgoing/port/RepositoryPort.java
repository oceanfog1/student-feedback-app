package com.project.feedback.infra.outgoing.port;

import com.project.feedback.domain.GitRepo;
import com.project.feedback.infra.outgoing.jpa.RepositoryEntity;
import com.project.feedback.infra.outgoing.jpa.UserEntity;

import java.util.List;
import java.util.Map;

public interface RepositoryPort {

    Map<Long, GitRepo> findRepositoryMapByName(String repoName);

    List<RepositoryEntity> findRepositoriesByUser(UserEntity user);
}
