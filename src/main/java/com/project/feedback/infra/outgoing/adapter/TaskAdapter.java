package com.project.feedback.infra.outgoing.adapter;

import com.project.feedback.domain.dto.course.TodayCourseInfo;
import com.project.feedback.infra.outgoing.jpa.TaskEntity;
import com.project.feedback.infra.outgoing.port.TaskPort;
import com.project.feedback.infra.outgoing.repository.TaskRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TaskAdapter implements TaskPort {
    private final TaskRepository taskRepository;

    public TaskAdapter(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<TaskEntity> findByTodayCourseInfo(TodayCourseInfo todayCourseInfo) {
        return taskRepository.findByCourseIdAndWeekAndDay(
                todayCourseInfo.getId(),
                todayCourseInfo.getWeek(),
                todayCourseInfo.getDay());
    }
}
