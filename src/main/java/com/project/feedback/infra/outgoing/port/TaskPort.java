package com.project.feedback.infra.outgoing.port;

import com.project.feedback.domain.dto.course.TodayCourseInfo;
import com.project.feedback.infra.outgoing.jpa.TaskEntity;

import java.util.List;

public interface TaskPort {

    List<TaskEntity> findByTodayCourseInfo(TodayCourseInfo todayCourseInfo);
}
