package com.project.feedback.application;

import com.project.feedback.domain.dto.course.TodayCourseInfo;

public interface CourseService {
    TodayCourseInfo getTodayCourseInfo(String userName);
}
