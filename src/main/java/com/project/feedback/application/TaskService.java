package com.project.feedback.application;

import com.project.feedback.domain.Task;
import com.project.feedback.domain.dto.course.TodayCourseInfo;
import com.project.feedback.domain.dto.task.TaskCreateRequest;
import com.project.feedback.domain.dto.task.TaskCreateResponse;
import com.project.feedback.domain.dto.task.TaskListDto;

import java.util.List;
import java.util.Optional;

public interface TaskService {
    List<TaskListDto> getTasks(TodayCourseInfo todayCourseInfo);
    Optional<Task> lastTask(long courseId);
    TaskCreateResponse createTask(TaskCreateRequest req);

}
