package com.project.feedback.application;

import com.project.feedback.domain.Course;
import com.project.feedback.domain.CourseStudent;
import com.project.feedback.domain.GitRepo;
import com.project.feedback.domain.User;
import com.project.feedback.domain.dto.CourseStudentRequest;
import com.project.feedback.domain.dto.course.TodayCourseInfo;
import com.project.feedback.infra.outgoing.jpa.CourseEntity;
import com.project.feedback.infra.outgoing.jpa.TaskEntity;
import com.project.feedback.infra.outgoing.jpa.UserEntity;
import com.project.feedback.infra.outgoing.jpa.UserTaskEntity;
import com.project.feedback.infra.outgoing.port.*;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CourseStudentService {

    private final UserPort userPort;
    private final CoursePort coursePort;
    private final TaskPort taskPort;
    private final UserTaskPort userTaskPort;
    private final RepositoryPort repositoryPort;

    public CourseStudentService(UserPort userPort, CoursePort coursePort, TaskPort taskPort, UserTaskPort userTaskPort, RepositoryPort repositoryPort) {
        this.userPort = userPort;
        this.coursePort = coursePort;
        this.taskPort = taskPort;
        this.userTaskPort = userTaskPort;
        this.repositoryPort = repositoryPort;
    }

    public CourseStudent getCourseStudent(CourseStudentRequest courseStudentRequest) {
        return getCourseStudent(courseStudentRequest.getTodayCourseInfo(), courseStudentRequest.getRepoName(), courseStudentRequest.getUserName());
    }


    /**
     * 로그인한 UserID를 기반으로 CourseStudent를 생성한다
     * @return CourseStudent
     */
    @Cacheable(value = "get_course_student", key = "#todayCourseInfo.id + '_' + #todayCourseInfo.week + '_' + #todayCourseInfo.dayOfWeek + '_' + #repoName")
    public CourseStudent getCourseStudent(TodayCourseInfo todayCourseInfo, String repoName, String userName) {

        UserEntity loginUser = userPort.findUserByUserName(userName);
        List<UserEntity> userEntities = userPort.findUsersByCourseId(todayCourseInfo.getId());
        CourseEntity courseEntity = coursePort.findCourseByUserId(loginUser);

        List<TaskEntity> taskEntities = taskPort.findByTodayCourseInfo(todayCourseInfo);
        List<UserTaskEntity> userTaskEntities = userTaskPort.findByUserEntitiesIn(userEntities);
        Map<Long, GitRepo> repoMap = repositoryPort.findRepositoryMapByName(repoName);
        return new CourseStudent(User.fromEntity(loginUser), Course.fromEntity(courseEntity), taskEntities,
                userEntities, userTaskEntities, repoMap);
    }
}
