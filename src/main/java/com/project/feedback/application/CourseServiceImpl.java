package com.project.feedback.application;

import com.project.feedback.domain.Course;
import com.project.feedback.domain.dto.course.*;
import com.project.feedback.domain.dto.task.TaskListDto;
import com.project.feedback.infra.outgoing.jpa.CourseEntity;
import com.project.feedback.infra.outgoing.jpa.CourseUserEntity;
import com.project.feedback.infra.outgoing.jpa.UserEntity;
import com.project.feedback.exception.CustomException;
import com.project.feedback.exception.ErrorCode;
import com.project.feedback.infra.outgoing.repository.CourseRepository;
import com.project.feedback.infra.outgoing.repository.CourseUserRepository;
import com.project.feedback.repository.CardCourseRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.C;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class CourseServiceImpl implements CourseService{

    private final CardCourseRepository cardCourseRepository;
    private final CourseRepository courseRepository;
    private final CourseUserRepository courseUserRepository;
    private final FindService findService;
    private final TaskService taskService;

    public List<CardCourseDto> listCardCourses() {
        return cardCourseRepository.findAll();
    }


    public List<Course> getCourses(Pageable pageable) {
        return courseRepository.findAll(pageable)
                .stream()
                .map(courseEntity -> Course.fromEntity(courseEntity)).collect(Collectors.toList());
    }

    public CourseListPage getCourses(String userName, Pageable pageable) {
        return new CourseListPage(getCourses(pageable));
    }

    public int getCourseLength(){
        return courseRepository.findAll().size();
    }

    public List<CourseDto> courses() {
        return courseRepository.findAll().stream()
            .map(courseEntity -> CourseDto.fromEntity(courseEntity)).collect(Collectors.toList());
    }

    public CourseCreateResponse createCourse(CourseCreateRequest req, String userName) {
        // UserName이 존재하는지 체크
        UserEntity writeUser = findService.findUserByUserName(userName);
        CourseEntity savedCourse = courseRepository.save(req.toEntity(writeUser));
        return CourseCreateResponse.of(savedCourse.getId(), savedCourse.getName());
    }

    public CourseEntity findByCourseName(String courseName){
        CourseEntity courseEntity = courseRepository.findByName(courseName)
            .orElseThrow(()-> new CustomException(ErrorCode.COURSE_NOT_FOUND));
        return courseEntity;

    }

    public CourseEntity findByCourseId(Long courseId){
        CourseEntity courseEntity = courseRepository.findById(courseId)
            .orElseThrow(()-> new CustomException(ErrorCode.COURSE_NOT_FOUND));
        return courseEntity;

    }

    public Course findByCourseId2(Long courseId){
        CourseEntity courseEntity = courseRepository.findById(courseId)
                .orElseThrow(()-> new CustomException(ErrorCode.COURSE_NOT_FOUND))
                ;
        return Course.fromEntity(courseEntity);

    }

    public void registerStudent(AddStudentRequest req) {
        CourseEntity course = findService.findCourseByName(req.getCourseName());

        //기수 등록해야하는 학생 list
        List<UserEntity> users = req.getUserList();
        CourseUserEntity courseUserEntity;
        for(UserEntity user : users){
            if(!courseUserRepository.findCourseEntityUserByUserId(user.getId()).isPresent()){
                courseUserEntity = new CourseUserEntity();
                courseUserEntity.setUser(user);
                courseUserEntity.setCourseEntity(course);
            }
            else{ // 존재 한다면
                courseUserEntity = courseUserRepository.findCourseEntityUserByUserId(user.getId()).orElseThrow(() -> new CustomException(ErrorCode.USER_COURSE_NOT_FOUND));
                courseUserEntity.setCourseEntity(course);
            }
            courseUserRepository.save(courseUserEntity);
        }
    }

    public void setDefaultCourse() {
        CourseCreateResponse courseCreateResponse;

        if (courseRepository.count() == 0) {
            CourseCreateRequest courseCreateRequest = CourseCreateRequest.builder()
                    .name("기본 기수1")
                    .status("CREATED")
                    .startDate(LocalDate.now())
                    .endDate(LocalDate.now().plusDays(1))
                    .description("")
                    .build();

            log.info("기본기수 등록 {}", courseCreateRequest);
            courseCreateResponse = createCourse(courseCreateRequest, "student");
        }

    }

    private CourseEntity getCourseEntity(String userName) {
        UserEntity loginUser = findService.findUserByUserName(userName);
        CourseEntity course = findService.findCourseByUserId(loginUser);
        return course;
    }



    /**
     * 오늘날짜에 task가 없다면 사용자가 속한 Course의 Task의 마지막 날짜를 불러온다.
     * */
    @Override
    public TodayCourseInfo getTodayCourseInfo(String userName) {
        CourseEntity course = getCourseEntity(userName);
        TodayCourseInfo todayCourseInfo = TodayCourseInfo.fromEntity(course);
        List<TaskListDto> tasks = taskService.getTasks(todayCourseInfo);
        if (tasks.size() == 0){
            taskService.lastTask(todayCourseInfo.getId()).ifPresent(task -> {
                log.info("task:{} {} {} id:{}", task.getWeek(), task.getDay(), task.getTitle());
                todayCourseInfo.setWeek(task.getWeek());
                todayCourseInfo.setDayOfWeek(Math.toIntExact(task.getDay()));
            });
        }

        return todayCourseInfo;
    }
}
