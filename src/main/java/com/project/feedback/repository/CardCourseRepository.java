package com.project.feedback.repository;

import com.project.feedback.domain.dto.course.CardCourseDto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardCourseRepository extends JpaRepository<CardCourseDto, Long> {
}
