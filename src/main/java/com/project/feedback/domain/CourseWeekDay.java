package com.project.feedback.domain;

public class CourseWeekDay {
    private Course course;
    private long week;
    private long day;

    public CourseWeekDay(Course course, long week, long day) {
        this.course = course;
        this.week = week;
        this.day = day;
    }
}
