package com.project.feedback.domain.dto.mainInfo;

import com.project.feedback.domain.GitRepo;
import com.project.feedback.domain.User;
import com.project.feedback.infra.outgoing.jpa.UserEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Builder
@Setter
public class StudentInfo {
    private Long id;
    private String realName;
    private String userName;
    private List<StatusInfo> status;
    private GitRepo gitRepo;

    public static StudentInfo of(User user, List<StatusInfo> status, GitRepo gitRepo) {
        return StudentInfo.builder()
            .id(user.getId())
            .realName(user.getRealName())
            .userName(user.getUserName())
            .gitRepo(gitRepo)
            .status(status)
            .build();
    }

    public static StudentInfo of(UserEntity user) {
        return StudentInfo.builder()
            .id(user.getId())
            .realName(user.getRealName())
            .userName(user.getUserName())
            .build();
    }
}
