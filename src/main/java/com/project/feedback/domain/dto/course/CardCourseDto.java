package com.project.feedback.domain.dto.course;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.UUID;

@Getter @Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "cardcoursedto")
public class CardCourseDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    private String image; // 이미지 파일명

    private String courseName;

    private String description;

    private String status;

    // DB는 이미지에 대한 정보만 저장한다.

    @Transient // 데이터베이스에 저장하지 않는 필드임을 지정합니다.
    private MultipartFile imageFile; // 이미지 파일을 받아오는 필드입니다


    // 이미지 파일을 저장하고 경로를 설정하는 메서드입니다.
    public void setImageFile(MultipartFile imageFile) {
        this.imageFile = imageFile;
        if (imageFile != null && !imageFile.isEmpty()) {
            try {
                String fileName = UUID.randomUUID().toString() + "_" + imageFile.getOriginalFilename();
                String imagePath = "images/" + fileName;
                this.image = imagePath;

                // 이미지 파일을 저장합니다.
                Path filePath = Paths.get("src/main/resources/static/" + imagePath);
                Files.copy(imageFile.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
                // 예외 처리를 적절히 수행합니다.
            }
        }
    }
}
