package com.project.feedback.domain.dto.course;

import com.project.feedback.domain.Course;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class CourseListPage {
    private List<Course> courses;
}
