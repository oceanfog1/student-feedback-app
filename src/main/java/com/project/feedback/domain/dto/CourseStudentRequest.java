package com.project.feedback.domain.dto;

import com.project.feedback.domain.dto.course.TodayCourseInfo;
import lombok.Getter;

@Getter
public class CourseStudentRequest {
    private TodayCourseInfo todayCourseInfo;
    private String repoName;
    private String userName;

    public CourseStudentRequest(TodayCourseInfo todayCourseInfo, String repoName, String userName) {
        this.todayCourseInfo = todayCourseInfo;
        this.repoName = repoName;
        this.userName = userName;
    }
}
