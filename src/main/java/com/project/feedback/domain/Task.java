package com.project.feedback.domain;

import com.project.feedback.infra.outgoing.jpa.TaskEntity;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Task {
    private long id;
    private String title;
    private Long week;
    private Long day;

    public static Task fromEntity(TaskEntity entity) {
        if(entity.getTaskStatus() == null){
            throw new RuntimeException("TaskStatus가 없습니다. TaskStatus는 반드시 넣어주세요.");
        }
        return Task.builder()
                .id(entity.getId())
                .title(entity.getTitle())
                .week(entity.getWeek())
                .day(entity.getDayOfWeek())
                .build();
    }
}
