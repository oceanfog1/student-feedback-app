package com.project.feedback.domain;

import com.project.feedback.domain.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UserTask {
    private long id;
    private long userId;
    private long taskId;
    private TaskStatus taskStatus;
}
