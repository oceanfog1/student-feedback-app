package com.project.feedback.domain;

import com.project.feedback.domain.dto.mainInfo.StatusInfo;
import com.project.feedback.domain.dto.mainInfo.StudentInfo;
import com.project.feedback.infra.outgoing.jpa.TaskEntity;
import com.project.feedback.infra.outgoing.jpa.UserEntity;
import com.project.feedback.infra.outgoing.jpa.UserTaskEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@Setter
public class CourseStudent {
    private User user;
    private Course course;
    private List<User> users;
    private List<Task> tasks;
    private List<UserTask> userTasks;
    private Map<Long, GitRepo> gitRepoMap;
    private List<StudentInfo> studentInfo;

    /**
     * CourseStudent생성
     * @param user 로그인한 사용자
     * @param course 사용자가 속한 코스
     * @param taskEntities 사용자가 오늘 받은 Task
     * @param userEntities 사용자가 속한 코스의 모든 인원
     * @param userTaskEntities 사용자와 Task의 매핑 정보
     * @param repoMap 사용자가 등록한 리포지토리
     */
    public CourseStudent(User user, Course course, List<TaskEntity> taskEntities, List<UserEntity> userEntities, List<UserTaskEntity> userTaskEntities, Map<Long, GitRepo> repoMap) {
        this.user = user;
        this.course = course;

        this.tasks = taskEntities.stream()
                .map(Task::fromEntity)
                .collect(Collectors.toList());
        this.users = this.userEntitiesToUsers(userEntities);

        this.userTasks = userTaskEntities.stream()
                .map(userTaskEntity -> UserTask.builder()
                        .id(userTaskEntity.getId())
                        .userId(userTaskEntity.getUser().getId())
                        .taskId(userTaskEntity.getTaskEntity().getId())
                        .taskStatus(userTaskEntity.getStatus())
                        .build())
                .collect(Collectors.toList());
        this.gitRepoMap = repoMap;


        this.studentInfo = this.makeStudentInfo();
    }

    private List<User> userEntitiesToUsers(List<UserEntity> userEntities) {

        return userEntities.stream()
                .filter(userEntity -> userEntity.getRole() == Role.ROLE_STUDENT) // Role.ROLE_STUDENT인 레코드만 분리
                .sorted(Comparator.comparing(UserEntity::getRealName))
                .map(User::fromEntity)
                .collect(Collectors.toList());
    }


    private List<StatusInfo> getStatusInfos(User user) {

        return this.tasks.stream()
                .map(task ->{
                    // filter 정보에 해당하는 task id 정보만 저장
                    UserTask userTaskEe = this.userTasks.stream()
                            .filter(userTask -> userTask.getUserId() == user.getId() && userTask.getTaskId() == task.getId())
                            .findFirst()
                            .orElse(UserTask.builder()
                                    .taskStatus(TaskStatus.CREATED)
                                    .build());
                    TaskStatus taskStatus = userTaskEe.getTaskStatus();
                    return StatusInfo.of(task.getId(), task.getTitle(), taskStatus.toString());
                })
                .collect(Collectors.toList());
    }

    private List<StudentInfo> makeStudentInfo() {

        List<StudentInfo> studentInfos = new ArrayList<>(); // 결과
        for(User user : this.users){
            //데이터 셋팅
            List<StatusInfo> statusInfo = getStatusInfos(user);

            GitRepo gitRepo = this.gitRepoMap.get(user.getId());
            studentInfos.add(StudentInfo.of(user, statusInfo, gitRepo));
        }

        if (this.user.getRole() != Role.ROLE_STUDENT) return studentInfos;

        for (int i = 0; i < studentInfos.size(); i++) {
            if (studentInfos.get(i).getUserName().equals(this.user.getUserName())) {
                studentInfos.add(0, studentInfos.remove(i));
                break;
            }
        }
        return studentInfos;
    }

    public List<StudentInfo> getStudentInfo() {
        return studentInfo;
    }

    public boolean isTaskEmpty() {
        return tasks.isEmpty();
    }
}
