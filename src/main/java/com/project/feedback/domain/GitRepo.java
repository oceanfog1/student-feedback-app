package com.project.feedback.domain;

import com.project.feedback.infra.outgoing.jpa.RepositoryEntity;
import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GitRepo {
    private String url;

    public static GitRepo fromEntity(RepositoryEntity entity) {
        return new GitRepo(entity.getAddress());
    }
}
