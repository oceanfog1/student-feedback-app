package com.project.feedback.domain;

import com.project.feedback.infra.outgoing.jpa.UserEntity;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class User {

    private long id;
    private String realName;
    private String userName;
    private Role role;

    public static User fromEntity(UserEntity entity) {

        if(entity.getRealName() == null) {
            throw new RuntimeException("RealName이 없습니다.");
        }

        return User.builder()
                .id(entity.getId())
                .realName(entity.getRealName())
                .userName(entity.getUserName())
                .role(entity.getRole())
                .build();
    }
}
